#!/usr/bin/python3

from helpdesk import HelpDesk
from mail_parser import MailParser
import pprint

#thread that polls regularly from help desk, parse the emails and fills a db

#function to be invoked manually or every X that performs the match
# and checks against SLAs.

if __name__=='__main__':
    
    help_desk = HelpDesk()
    mail_parser = MailParser()
    
    for mail in help_desk.getUnreadEmails():
        ok, parsed_mail = mail_parser.parse(mail)
        if ok:
            pprint.pprint(parsed_mail)
            print('\n')
        else:
            print('Encountered error in mail parsing!')