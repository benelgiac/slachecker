import re
import dateparser
import logging

# -----------------------------------------------------------
#Dictionaries holding static informations that realistically 
# should be in a separate database

#Common header parsing rules (valid for every provider)
HEADER_PARSING_RULES={
    'Provider': r'Subject:.*from\s*(.*)\sto'
}

#Provider-spacific parsing rules
BODY_PARSING_RULE_SET={
    'Gene': {},
    'NiceProvider': {},
    'IHateThisProvider': {},
    'Fiber Provider': {
        'PW ID': r'PW Reference number:\s*(.*)',
        'Start': r'Start Date and Time:\s*(.*)',
        'End': r'End Date and Time:\s*(.*)',
        'Service ID': r'Service ID:\s*(.*)',
    }
}

#Default body parsing rules, to be used for providers 
# that do not have specific rules
DEFAULT_BODY_PARSING_RULE_SET = {
        'PW ID': r'PW Reference number:\s*(.*)',
        'Start': r'Start Date and Time:\s*(.*)',
        'End': r'End Date and Time:\s*(.*)',
        'Service ID': r'Service ID:\s*(.*)',
    }

#Parser functions for specific field conversion from string
# to the appropriate data type
BODY_FIELD_PARSERS= {
    'PW ID': lambda x: x,
    'Start': dateparser.parse,
    'End': dateparser.parse,
    'Service ID': lambda x: x
}

# -----------------------------------------------------------

class MailParser(object):
    '''
    A class to parse provider's maintenance emails.

    Methods
    -------
    parse(mail)
        Parses a single mail and returns a dictionary with 
        the results. Logs any errors and unparsed emails.
    '''

    UNPARSED_MAIL_LOG='unparsed.txt'

    def __init__(self):
        self.logger = logging.getLogger(type(self).__name__)

        #Realistically this would be some sort of database connection
        # init.
        self.body_rule_set = BODY_PARSING_RULE_SET

    def parse(self, mail):
        '''
        Method to parse a mail, provided as a simple string.

        Returns a tuple (success, parsed_mail).

        If parsing is successful, parsed_mail is a dictionary of k,v, where:
        k is the parsed field name 
        v is the parsed value (with appropriate data type)

        If parsing is not successful, parsed_mail is partially filled with the 
        field parsed up to the first error encountered. An error is logged and
        the faulty parser/regular expression is logged to a separate file for 
        further human-based evaluation.

        This method support the case where every provider can use its own
        format of email body. As such, parsing rules are looked up in a provider
        rule database.
        
        If the provider has no specific rules in the db, common ones are used.

        POSSIBLE IMPROVEMENTS:
        - the parsed mail is not structured at all. The list of parsed fields 
          are entirely dependent on the rules present in the database. There 
          could be the need to categorize somehow fields into 'mandatory' and
          'optional' when more fields are added in the rule set, to avoid 
          unnecessarily mail parsing failures.

        - BODY_FIELD_PARSERS at the moment holds directly a function to parse
          a text field into a specific data type. 
          This should be better embedded in the rule set: each field should
          have his own data type description 'text', 'integer', 'datetime' 
          and parser would be picked up by the application logic based on that. 

        '''
        self.parsed_mail = {}

        ok = self.parseHeader(mail)

        #It is only possible to parse the body if header was parsed
        # successfully
        if ok: 
            ok = self.parseBody(mail)

        if ok:     
            ok = self.parseAttachment(mail)

        return ok, self.parsed_mail

    def parseHeader(self, mail):
        for field, regex in HEADER_PARSING_RULES.items():
            m = re.search(regex, mail)
            if m:
                self.parsed_mail[field] = m.group(1)
            else:
                self.logUnparsed(mail, field, regex)
                return False

        return True

    def parseBody(self, mail):
        #Rules for parsing the email body for this particular provider
        # are taken from a rules database. In this simple example we 
        # are looking up into a hard-wired dictionary.
        #If rules for this provider name are not present, default ones
        # are used.
        rules = self.body_rule_set.get(self.parsed_mail['Provider'], DEFAULT_BODY_PARSING_RULE_SET)
        
        for field, regex in rules.items():
            m = re.search(regex, mail)
            if m:
                #About the next instruction...
                # there should a better mechanism in place to check if the 
                # parser has actually been able to parse the field. In this
                # exercise it simply is None or valid.
                #Also, I make the simple assumption that every email only holds
                # information about work on one circuit/service only. If that should not
                # be the case, we should use method "findall" to search for all 
                # occurrences and create a list of service/circuit being impacted
                self.parsed_mail[field] = BODY_FIELD_PARSERS[field](m.group(1))

                if self.parsed_mail[field] is None:
                    #Logs the parser function that has failed
                    self.logUnparsed(mail, field, f'{BODY_FIELD_PARSERS[field].__name__}({m.group(1)})')
                    return False
            else:
                self.logUnparsed(mail, field, regex)
                return False

        return True

    def parseAttachment(self, mail):
        return True

    def logUnparsed(self, mail, field, regex):
        #Logs the entire mail body and the particular instruction that has failed to a file.
        # Of course in the reality this would be stored in a database accessible by a 
        # user probably through a dashboard or web page.
        self.logger.error('Unparsed email encountered! Please inspect file {}!'.format(self.UNPARSED_MAIL_LOG))
        with open(self.UNPARSED_MAIL_LOG, 'w+') as f:
            f.write('--------- START UNPARSED MAIL ----------------------------\n')
            f.write(f'Cannot extract field \'{field}\' using expression \'{regex}\' in the following mail:\n')
            f.write('\n')
            f.write(mail)
            f.write('\n')
            f.write('--------- END UNPARSED MAIL ------------------------------\n')