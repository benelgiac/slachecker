## SLA Checker application

Exercise showing code to parse emails from an help desk application.

Dependencies: `dateparser` (install via `pip install -r requirements.txt`).

How to run: 
```bash
chmod +x SLAChecker.py
./SLAChecker.py
``` 
This script would print all the emails successfully parsed and will 
instead log to separate files all the failures.

The presence of the help desk application is stubbed in the helpdesk module.
This just loads example emails from a list of txt files at init and 
provides a method to get email bodies one by one.

The mail_parser module provides a method to parse a single email by
using a set of provider-based rules based on regular expressions. 
The rules are hard-coded into the module itself.