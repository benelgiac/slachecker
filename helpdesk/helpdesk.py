import os

class HelpDesk(object):
    '''Simple mock-up class that would imitate the presence of
    an API for retrieving unread emails from and HelpDesk app.
    In this case the emails are loaded at init and are taken
    from a list of .txt files provided with the project as an
    example.
    '''

    MAIL_FILES=['./provider_email.txt', './unparsable_email.txt']

    def __init__(self):
        self.current_dir = os.path.dirname(__file__)
        self.unread_list=[]
        for filename in self.MAIL_FILES:
            with open(os.path.join(self.current_dir, filename), 'r') as f:
                self.unread_list.append(f.read())

    def getUnreadEmails(self):
        for mail in self.unread_list:
            yield mail

        self.unread_list=[]